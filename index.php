<?php


class Car{
    private $speed = 0;

    function __construct(){
        echo "I am a new car<br>";
    }

    function drive(){
        if($this->speed == 0){
            echo "I am a slow car<br>";
            return;
        }
        echo "<b>Wrooom</b> Im driving with speed " . $this->speed . "mps<br>";
    }

    function setSpeed($speed = 10){
        $this->speed = $speed;
    }

    function accelerate(){
        for($i = 0; $i < 10; $i++){
            $this->drive();
            $this->setSpeed($this->speed + 10);
        }
    }
}


$car = new Car;
$car->accelerate();







